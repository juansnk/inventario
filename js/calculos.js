$(document).on('ready', funcMain);


 function funcMain()
 {
     $("#add_row").on('click',newRowTable);
 
     $("loans_table").on('click','.fa-eraser',deleteProduct);
     $("loans_table").on('click','.fa-edit',editProduct);
 
     $("body").on('click',".fa-eraser",deleteProduct);
     $("body").on('click',".fa-edit",editProduct);
 }
//FUncion para eliminar fila de la tabla
 function funcEliminarProductosso(){
	//Obteniendo la fila que se esta eliminando
	var a=this.parentNode.parentNode;

	var cantidadEntrada=a.getElementsByTagName("td")
	console.log(a);

	$(this).parent().parent().fadeOut("slow",function(){$(this).remove();});
}

function deleteProduct(){
	//Guardando la referencia del objeto presionado
	var _this = this;
	//Obtener las filas los datos de la fila que se va a elimnar
	var array_fila=getRowSelected(_this);

	calculateTotals(array_fila[3],array_fila[4],array_fila[5],array_fila[6],array_fila[7],2)

	$(this).parent().parent().fadeOut("slow",function(){$(this).remove();});
}
 
//funcion para editar cada fila
function editProduct(){
	var _this = this;;
	var array_fila=getRowSelected(_this);
	console.log(array_fila[0]+" - "+array_fila[1]+" - "+array_fila[2]+" - "+array_fila[3]+" - "+array_fila[4]+" - "+array_fila[5]+" - "+array_fila[6]+" - "+array_fila[7]+" - "+array_fila[8]+" - "+array_fila[9]+" - "+array_fila[10]+" - "+array_fila[11]);
}

	//b=(fila).(obtener elementos de clase columna y traer la posicion 0).(obtener los elementos de tipo parrafo y traer la posicion0).(contenido en el nodo)
function getRowSelected(objectPressed){
	//Obteniendo la linea que se esta eliminando
	var a=objectPressed.parentNode.parentNode;

	var nombre=a.getElementsByTagName("td")[0].getElementsByTagName("p")[0].innerHTML;
	var fecha=a.getElementsByTagName("td")[1].getElementsByTagName("p")[0].innerHTML;
	var numero=a.getElementsByTagName("td")[2].getElementsByTagName("p")[0].innerHTML;
	var codigo=a.getElementsByTagName("td")[3].getElementsByTagName("p")[0].innerHTML;
	var descripcion=a.getElementsByTagName("td")[4].getElementsByTagName("p")[0].innerHTML;
	var cantidadEntrada=a.getElementsByTagName("td")[5].getElementsByTagName("p")[0].innerHTML;
	var cantidadSalida=a.getElementsByTagName("td")[6].getElementsByTagName("p")[0].innerHTML;
    var valorUnitarioEntrada=a.getElementsByTagName("td")[7].getElementsByTagName("p")[0].innerHTML;
    var totalEntrada=a.getElementsByTagName("td")[8].getElementsByTagName("p")[0].innerHTML;
    var totalSalida=a.getElementsByTagName("td")[9].getElementsByTagName("p")[0].innerHTML;
    var cantidadSaldo=a.getElementsByTagName("td")[10].getElementsByTagName("p")[0].innerHTML;
    var totalSaldo=a.getElementsByTagName("td")[11].getElementsByTagName("p")[0].innerHTML;
	var array_fila = [nombre, fecha, numero, codigo, descripcion, cantidadEntrada, cantidadSalida, valorUnitarioEntrada,totalEntrada, totalSalida, cantidadSaldo, totalSaldo];

	return array_fila;
	//console.log(numero+' '+codigo+' '+descripcion);
}
//funcion donde se recoje los valores de nuestro formulario y se realzian los respectivos calculos y 
//añade una fila a nuestrat abla
function newRowTable()
{
	
    var nombre=document.getElementById("name").value;
    var fecha=document.getElementById("fecha").value;
	var numero=document.getElementById("numero").value;
	var codigo=document.getElementById("codigo").value;
	var descripcion=document.getElementById("descripcion").value;
    var cantidadEntrada=document.getElementById("cantidadEntrada").value;
    var cantidadSalida=document.getElementById("cantidadSalida").value;
	var valorUnitarioEntrada=document.getElementById("precio").value;
    var totalEntrada=parseFloat(cantidadEntrada)*parseFloat(valorUnitarioEntrada);
    var totalSalida=parseFloat(cantidadSalida)*parseFloat(valorUnitarioEntrada);
	var cantidadSaldo=cantidadEntrada - cantidadSalida;
	var totalSaldo=parseFloat(totalEntrada)-parseFloat(totalSalida);
	var saldCantidadTotal=document.getElementById("saldCantidadTotal").innerHTML;
	
	if(cantidadSalida > saldCantidadTotal){
		alert("La cantidad de salida es mayor a la del saldo");
		return false;
	}else{
	var name_table=document.getElementById("content_table");

    var row = name_table.insertRow(0+1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7);
    var cell9 = row.insertCell(8);
    var cell10 = row.insertCell(9);
    var cell11 = row.insertCell(10);
    var cell12 = row.insertCell(11);
	var cell13 = row.insertCell(12);


    cell1.innerHTML = '<p name="codigo" class="non-margin">'+codigo+'</p>';
    cell2.innerHTML = '<p name="fecha" class="non-margin">'+fecha+'</p>';
    cell3.innerHTML = '<p name="name" class="non-margin">'+nombre+'</p>';
    cell4.innerHTML = '<p name="descuento" class="non-margin">'+descripcion+'</p>';
    cell5.innerHTML = '<p name="numero" class="non-margin">'+numero+'</p>'; 
    cell6.innerHTML = '<p name="precio" class="non-margin">'+valorUnitarioEntrada+'</p>';  
    cell7.innerHTML = '<p name="cantidadEntrada" class="non-margin">'+cantidadEntrada+'</p>';
    cell8.innerHTML = '<p name="totalEntrada" class="non-margin">'+totalEntrada+'</p>';
    cell9.innerHTML = '<p name="cantidadSalidalass="non-margin">'+cantidadSalida+'</p>';
    cell10.innerHTML = '<p name="valorTotalSalida" class="non-margin">'+totalSalida+'</p>';
    cell11.innerHTML = '<p name="cantidadTotalSaldo" class="non-margin">'+cantidadSaldo+'</p>';
    cell12.innerHTML = '<p name="totalSaldo" class="non-margin">'+totalSaldo+'</p';
	cell13.innerHTML = '<span class="icon fa-eraser" style="color:red">Delete</span>';

	
    //Para calcular los totales enviando los parametros
	calculateTotals(cantidadEntrada, totalEntrada, cantidadSalida, totalSalida ,cantidadSaldo, totalSaldo, 1);
	}
}
//funcion que aloja el resultado de los calculos y los ingresa en los td de nuestra tabla 
function calculateTotalsBySumColumn(){
	var total_cantidadEntrada=0;
	var array_cantidadesEntrada=document.getElementsByName("cantidadEntrada");
	for (var i=0; i<array_cantidades.length; i++) {
		total_cantidadEntrada+=parseFloat(array_cantidadesEntrada[i].innerHTML);
	}
	document.getElementById("entCantidadTotal").innerHTML=total_cantidadEntrada;


	var valorTotalEntrada=0;
	var array_valorTotalEntrada=document.getElementsByName("totalEntrada");
	for (var i=0; i<array_valorTotalEntrada.length; i++) {
		valorTotalEntrada+=parseFloat(array_valorTotalEntrada[i].innerHTML);
	}
	document.getElementById("entTotal").innerHTML=valorTotalEntrada;


	var cantidadSalidas=0;
	var array_cantidadSalidas=document.getElementsByName("cantidadSalidas");
	for (var i=0; i<array_cantidadSalidas.length; i++) {
		cantidadSalidas+=parseFloat(array_cantidadSalidas[i].innerHTML);
	}
	document.getElementById("salCantidadTotal").innerHTML=cantidadSalidas;


	var valorTotalSalida=0;
	var array_valorTotalSalida=document.getElementsByName("valorTotalSalida");
	for (var i=0; i<array_valorTotalSalida.length; i++) {
		valorTotalSalida+=parseFloat(array_valorTotalSalida[i].innerHTML);
	}
	document.getElementById("salValorTotal").innerHTML=valorTotalSalida;

	var cantidadTotalSaldo=0;
	var array_cantidadTotalSaldo=document.getElementsByName("cantidadTotalSaldo");
	for (var i=0; i<array_cantidadTotalSaldo.length; i++) {
		cantidadTotalSaldo+=parseFloat(array_cantidadTotalSaldo[i].innerHTML);
	}
    document.getElementById("saldCantidadTotal").innerHTML=cantidadTotalSaldo;
    
    var totalSaldos=0;  
	var array_totalSaldo=document.getElementsByName("totalSaldo");
	for (var i=0; i<array_totalSaldo.length; i++) {
		totalSaldos+=parseFloat(array_totalSaldo[i].innerHTML);
	}
	document.getElementById("saldValorTotal").innerHTML=totalSaldos;

	var promedioPonderado=parseFloat(totalSaldos)/parseFloat(cantidadTotalSaldo).innerHTML;  

	document.getElementById("proPonderado").innerHTML=promedioPonderado;
}

//funcion que que ejecuta depende la la accion 
function calculateTotals(cantidadEntrada, totalEntrada, cantidadSalida, totalSalida ,cantidadSaldo, totalSaldo, accion){
	var entCantidadTotal=parseFloat(document.getElementById("entCantidadTotal").innerHTML);
	var entTotal=parseFloat(document.getElementById("entTotal").innerHTML);
	var salCantidadTotal=parseFloat(document.getElementById("salCantidadTotal").innerHTML);
	var salValorTotal=parseFloat(document.getElementById("salValorTotal").innerHTML);
    var saldCantidadTotal=parseFloat(document.getElementById("saldCantidadTotal").innerHTML);
	var saldValorTotal=parseFloat(document.getElementById("saldValorTotal").innerHTML);
	
	var promedioPonderado=parseFloat(document.getElementById("proPonderado").innerHTML);

	if (accion==1) {
		document.getElementById("entCantidadTotal").innerHTML=parseFloat(entCantidadTotal)+parseFloat(cantidadEntrada);
		document.getElementById("entTotal").innerHTML=parseFloat(entTotal)+parseFloat(totalEntrada);
		document.getElementById("salCantidadTotal").innerHTML=parseFloat(salCantidadTotal)+parseFloat(cantidadSalida);
		document.getElementById("salValorTotal").innerHTML=parseFloat(salValorTotal)+parseFloat(totalSalida);
        document.getElementById("saldCantidadTotal").innerHTML=parseFloat(saldCantidadTotal)+parseFloat(cantidadSaldo);
		document.getElementById("saldValorTotal").innerHTML=parseFloat(saldValorTotal)+parseFloat(totalSaldo);
		var saldCantidadTotall = parseFloat(saldCantidadTotal)+parseFloat(cantidadSaldo);
		var saldValorTotall =  parseFloat(saldValorTotal)+parseFloat(totalSaldo);
		var proPondera = saldValorTotall/ saldCantidadTotall;
		document.getElementById("proPonderado").innerHTML=parseFloat(proPondera);
	}else if(accion==2){
		document.getElementById("entCantidadTotal").innerHTML=parseFloat(entCantidadTotal)-parseFloat(cantidadEntrada);
		document.getElementById("entTotal").innerHTML=parseFloat(entTotal)-parseFloat(totalEntrada);
		document.getElementById("salCantidadTotal").innerHTML=parseFloat(salCantidadTotal)-parseFloat(cantidadSalida);
		document.getElementById("salValorTotal").innerHTML=parseFloat(salValorTotal)-parseFloat(totalSalida);
        document.getElementById("saldCantidadTotal").innerHTML=parseFloat(saldCantidadTotal)-parseFloat(cantidadSaldo);
		document.getElementById("saldValorTotal").innerHTML=parseFloat(saldValorTotal)+parseFloat(totalSaldo);
		document.getElementById("proPonderado").innerHTML=parseFloat(promedioPonderado)+parseFloat(proPonderado);
	}else{
		alert('Accion Invalida');
	}
}



function format(input)
{
	var num = input.value.replace(/\,/g,'');
	if(!isNaN(num)){
		input.value = num;
	}
	else{ alert('Solo se permiten numeros');
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}
/*
function calcular(){
     //valores de entradas
     //recibir en varaibles la cantidad y el valor unitario de entrada y realizar la multiplicacion
    var entCantidad = document.getElementById("entCantidad").value;
    var entValorUnitario = document.getElementById("entValorUnitario").value;
    var entTotal = entCantidad * entValorUnitario;
    //Si es vacio o null es 0
    var total1 = document.getElementById("entTotal").innerHTML;
    total1 = (total1 == null || total1 == undefined || total1 == "") ? 0 : total1;
    document.getElementById("entTotal").innerHTML = '$'+entTotal;

    //valores de salidas
    var salCantidad = document.getElementById("salCantidad").value;
    var salValorUnitario = document.getElementById("salValorUnitario").value;
    var salTotal = salCantidad * salValorUnitario;
    var total2 = document.getElementById("entTotal").innerHTML;
    total2 = (total2 == null || total2 == undefined || total2 == "") ? 0 : total2;
    document.getElementById("salTotal").innerHTML = '$'+salTotal;

    //valores saldo
    var saldCantidad = entCantidad- salCantidad;
    var saldTotal = entTotal - salTotal;
    var total3 = document.getElementById("saldCantidad").innerHTML;
    total3 = (total3 == null || total3 == undefined || total3 == "") ? 0 : total2;
    document.getElementById("saldCantidad").innerHTML = saldCantidad;
    var total4 = document.getElementById("saldTotal").innerHTML;
    total2 = (total4 == null || total4 == undefined || total4 == "") ? 0 : total4;
    document.getElementById("saldTotal").innerHTML = '$'+saldTotal;

    //sumar cantidad de entradas
    var valortotal = (entradas[i].ValorEnt+(Salida[i].ValUnitariov*Salida[i].CantidadV));
      var cantidadTotal = (entradas[i].Cantidad+Salida[i].CantidadV);
      var promPond = valortotal/cantidadTotal;

}
*/